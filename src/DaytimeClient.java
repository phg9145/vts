import java.rmi.Naming;

public class DaytimeClient {
    public static void main(String args[]) throws Exception {
        
        String host = args[0];
        //String text = args[1];
        
        Daytime remote = (Daytime) Naming.lookup("//" + host + "/daytime");
        String received = remote.getDaytime();
        System.out.println(received);
    }
}