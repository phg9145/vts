import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;

public class DaytimeImpl extends UnicastRemoteObject implements Daytime{
    public DaytimeImpl() throws RemoteException{};
    
    public String getDaytime() {
        return "Local time is \t" + LocalDateTime.now();
    }
}